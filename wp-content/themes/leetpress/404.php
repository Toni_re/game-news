<?php get_header(); ?>

	<!-- BEGIN MAIN WRAPPER -->
	<div id="main-wrapper">
	
		<!-- BEGIN MAIN -->
		<div id="main">
		
			<div id="post">
			
				<h3 class="section-title">Error 404 - Not Found</h3>
				
				<div class="post-entry">
				
					<p>La página solicitada no ha sido encontrada.</p>
					
				</div>
			
			</div>
			
		</div>
		<!-- END MAIN -->

<?php get_sidebar(); ?>

<?php get_footer(); ?>