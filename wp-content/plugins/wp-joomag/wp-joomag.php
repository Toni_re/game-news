<?php
/*
 Joomag WordPress Plugin
 ==============================================================================

 This plugin will allow you to embed Joomag magazines to your posts or pages on your Wordpress blog/website.

 Info for WordPress:
 ==============================================================================
 Plugin Name: WP Joomag
 Plugin URI: http://www.joomag.com/
 Description: Embed Joomag publications inside a post
 Author: Joomag.
 Version: 1.5.6
 Author URI: http://www.joomag.com

*/

function joomag_mag_filter($content)
{
    $content = preg_replace_callback('/\[joomag ([^]]*)\]/i', 'joomag_get_magazine', $content);
    return $content;
}

function joomag_bookshelf_filter($content)
{
    $content = preg_replace_callback('/\[joomag_bookshelf ([^]]*)\]/i', 'joomag_get_bookshelf', $content);
    return $content;
}

function _j_getValueWithDefault($regex, $params, $default)
{
    $matchCount = preg_match_all($regex, $params, $matches);
    if ($matchCount) {
        return $matches[1][0];
    } else {
        return $default;
    }
}

function joomag_get_magazine($matches)
{
    $height				 =	_j_getValueWithDefault('/(?:^|[\s]+)height=([\S]*)/i', $matches[1], 272);
    $width				 =	_j_getValueWithDefault('/(?:^|[\s]+)width=([\S]*)/i', $matches[1], 420);
    $pageNumber			 =	_j_getValueWithDefault('/(?:^|[\s]+)pageNumber=([\S]*)/i', $matches[1], 1);
    $magazineId			 =	_j_getValueWithDefault('/(?:^|[\s]+)magazineId=([\S]*)/i', $matches[1], '');
    $title				 =	_j_getValueWithDefault('/(?:^|[\s]+)title=([\S]*)/i', $matches[1], '');
    $backgroundColor	 =	_j_getValueWithDefault('/(?:^|[\s]+)backgroundColor=([\S]*)/i', $matches[1], '');
    $backgroundImg		 =	_j_getValueWithDefault('/(?:^|[\s]+)backgroundImage=([\S]*)/i', $matches[1], '');
    $toolbar		     =	_j_getValueWithDefault('/(?:^|[\s]+)toolbar=([\S]*)/i', $matches[1], '');
    $autoFlip		     =	_j_getValueWithDefault('/(?:^|[\s]+)autoFlip=([\S]*)/i', $matches[1], '');
    $autoFit			 =	_j_getValueWithDefault('/(?:^|[\s]+)autoFit=([\S]*)/i', $matches[1], 'false') == 'true' ? true : false;

    $embedCodeStr =  '<iframe name="Joomag_embed_${UUID}"'.
        ' style="width:${width};height:${height}" width="${width}" height="${height}" hspace="0" vspace="0" frameborder="0" '.
        ' src="${magURL}?page=${startPage}&e=1${otherOptions}"></iframe>' ;

    $domain = 'www.joomag.com';

    $viewerURL = '//' . $domain . '/magazine/' . $title . '/' . $magazineId;

    $embedOpts = array();
    if($toolbar != '')
    {
        switch( $toolbar ) {
            case 'none':
                array_push($embedOpts, 'noToolbar');
                break;
            case 'transparent':
                array_push($embedOpts, 'none');
                break;
            default:
                array_push($embedOpts, "solid,{$toolbar}");
                break;
        }
    } else {
        array_push($embedOpts, '');
    }

    if($backgroundColor != '')
    {
        $bgColors = explode(',', $backgroundColor);
        if( $backgroundColor == 'transparent' ) {
            array_push($embedOpts, 'none');
        } elseif( is_array($bgColors) && count($bgColors) == 2 ) {
            array_push($embedOpts, "gradient,{$backgroundColor}");
        } else {
            array_push($embedOpts, "solid,{$backgroundColor}");
        }
    } else if( $backgroundImg != '' ) {
        array_push($embedOpts, "image,{$backgroundImg},fill");
    } else {
        array_push($embedOpts, '');
    }

    $embedOptsStr = '&embedInfo=' . implode(';', $embedOpts);
    if( is_numeric($autoFlip) ) {
        $embedOptsStr .= "&autoFlipDelay={$autoFlip}";
    }

    if( $autoFit == true ) {
        $embedCodeStr = str_replace('${width}', '100%', $embedCodeStr);
        $embedCodeStr = str_replace('${height}', '100%', $embedCodeStr);}
    else {
        $embedCodeStr = str_replace('${width}', $width.'px', $embedCodeStr);
        $embedCodeStr = str_replace('${height}', $height.'px', $embedCodeStr);
    }

    $embedCodeStr = str_replace('${startPage}', $pageNumber, $embedCodeStr);

    $embedCodeStr = str_replace('${otherOptions}', $embedOptsStr, $embedCodeStr);

    $UUID = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );

    $embedCodeStr = str_replace('${magURL}', $viewerURL, $embedCodeStr);
    $embedCodeStr = str_replace('${UUID}', $UUID, $embedCodeStr);

    return $embedCodeStr;
}

function joomag_get_bookshelf($matches)
{
    $height				 =	_j_getValueWithDefault('/(?:^|[\s]+)height=([\S]*)/i', $matches[1], 460);
    $width				 =	_j_getValueWithDefault('/(?:^|[\s]+)width=([\S]*)/i', $matches[1], 450);
    $magazineId			 =	_j_getValueWithDefault('/(?:^|[\s]+)magazineId=([\S]*)/i', $matches[1], '');
    $title				 =	_j_getValueWithDefault('/(?:^|[\s]+)title=([\S]*)/i', $matches[1], '');
    $cols				 =	_j_getValueWithDefault('/(?:^|[\s]+)cols=([\S]*)/i', $matches[1], 3);
    $rows				 =	_j_getValueWithDefault('/(?:^|[\s]+)rows=([\S]*)/i', $matches[1], 2);

    $embedCodeStr =  '<iframe name="Joomag_embed_${UUID}"'.
        ' style="width:${width};height:${height}" width="${width}" height="${height}" hspace="0" vspace="0" frameborder="0" '.
        ' src="${bookshelfURL}&cols=${cols}&rows=${rows}"></iframe>' ;

    $domain = 'www.joomag.com';

    $bookshelfURL = '//' . $domain . '/Frontend/embed/bookshelf/index.php?UID=' . $magazineId;

    $UUID = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand(0, 0xffff), mt_rand(0, 0xffff),
        mt_rand(0, 0xffff),
        mt_rand(0, 0x0fff) | 0x4000,
        mt_rand(0, 0x3fff) | 0x8000,
        mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
    );

    $embedCodeStr = str_replace('${bookshelfURL}', $bookshelfURL, $embedCodeStr);
    $embedCodeStr = str_replace('${width}', $width.'px', $embedCodeStr);
    $embedCodeStr = str_replace('${height}', $height.'px', $embedCodeStr);
    $embedCodeStr = str_replace('${cols}', $cols, $embedCodeStr);
    $embedCodeStr = str_replace('${rows}', $rows, $embedCodeStr);
    $embedCodeStr = str_replace('${UUID}', $UUID, $embedCodeStr);

    return $embedCodeStr;
}


add_filter('the_content', 'joomag_mag_filter');
add_filter('the_content', 'joomag_bookshelf_filter');
?>
